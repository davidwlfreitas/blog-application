# **Blog Application** #

This project is a simple blog application for demonstration purpose with Laravel Framework 5.6, MongoDB and Vue.js.

PS:. Some functions needed to be fixed. if I had some more time for sure I'd be fix all the issues.

### Getting Started

1) Clone Repo

You can clone a repo from: [BlogApplication](https://bitbucket.org/davidwlfreitas/blog-application)

2) Run composer install

Run `composer install` command to install required dependencies. Once everything is installed, you are ready to use Blog Application template.

### **Configuration**

1) If your prefer put your own models, feel free to change to run your own migrations, but if you prefer using the original models just run the migrations.

```php

php artisan migrate

```
2) Run the migrate seeds files.

```php

php artisan migrate --seed

```

## **Running the tests**

For tests just run vendor/bin/phpunit

## **Built With**

* [Media Library](https://github.com/spatie/laravel-medialibrary/)
* [Laravel Blog Model](https://github.com/guillaumebriday/laravel-blog)

## **Contributing guidelines**

Support follows PSR-1 and PSR-4 PHP coding standards, and semantic versioning.

Please report any issue you find in the issues page.
Pull requests are welcome.

## **Authors**

* **David Freitas** - [davidwlfreitas](https://bitbucket.org/davidwlfreitas)

## **Documentation**

* Add PHPDoc blocks for all classes, methods, and functions
* Omit the `@return` tag if the method does not return anything
* Add a blank line before `@param`, `@return` and `@throws`

## **License**

Blog Application is free software distributed under the terms of the [MIT](https://opensource.org/licenses/MIT) license.