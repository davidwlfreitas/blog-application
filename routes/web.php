<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('posts/{id}', 'PostController@show')->name('posts.show.home');
Route::resource('users', 'UserController')->only('show');
Route::get('/', 'PostController@index')->name('home');
Route::get('/posts/feed', 'PostFeedController@index')->name('posts.feed');

Route::prefix('admin')->group(function()
{
    Route::get('/', 'AdminController@index');
     Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
     Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

     Route::group( ['middleware' => 'auth:admin'], function()
     {
         Route::get('dashboard', 'Admin\ShowDashboard')->name('admin.dashboard');

         // Posts
         Route::get('posts', 'Admin\PostController@index')->name('admin.posts.index');
         Route::get('posts/add', 'Admin\PostController@create')->name('admin.posts.create');
         Route::post('posts/add', 'Admin\PostController@store')->name('admin.posts.store');
         Route::get('posts/edit/{id}', 'Admin\PostController@edit')->name('admin.posts.edit');
         Route::post('posts/update', 'Admin\PostController@update')->name('admin.posts.update');
         Route::get('posts/{id}', 'Admin\PostController@destroy')->name('admin.posts.destroy');

         // Comments
         Route::get('comments', 'Admin\CommentController@index')->name('admin.comments.index');
         Route::get('comments/edit/{id}', 'Admin\CommentController@edit')->name('admin.comments.edit');
         Route::post('comments/edit/{id}', 'Admin\CommentController@update')->name('admin.comments.update');
         Route::post('comments/{id}', 'Admin\CommentController@destroy')->name('admin.comments.destroy');

         // Users
         Route::get('users', 'Admin\UserController@index')->name('admin.users.index');
         // Route::get('users/add', 'Admin\UserController@create')->name('admin.users.create');
         // Route::post('users/add', 'Admin\UserController@store')->name('admin.users.store');
         Route::get('users/edit/{id}', 'Admin\UserController@edit')->name('admin.users.edit');
         Route::post('users/update', 'Admin\UserController@update')->name('admin.users.update.home');
         // Route::post('users/{id}', 'Admin\UserController@destroy')->name('admin.users.destroy');

         // Media
         // Route::get('media', 'Admin\MediaLibraryController@index')->name('admin.media.index');
         // Route::get('media/add', 'Admin\MediaLibraryController@create')->name('admin.media.create');
         // Route::post('media/add', 'Admin\MediaLibraryController@store')->name('admin.media.store');
         // Route::get('media/edit/{id}', 'Admin\MediaLibraryController@edit')->name('admin.media.edit');
         // Route::post('media/edit/{id}', 'Admin\MediaLibraryController@update')->name('admin.media.update');
         // Route::post('media/{id}', 'Admin\MediaLibraryController@destroy')->name('admin.media.destroy');

         Route::delete('/posts/{post}/thumbnail', 'Admin\PostThumbnailController@destroy')->name('posts_thumbnail.destroy');
     });
});
