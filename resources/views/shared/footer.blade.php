<nav class="navbar navbar-dark bg-dark position-absolute footer-container">
    <div class="container justify-content-center">
        <ul class="navbar-nav text-center">
            <li class="nav-item text-white m-3">
                Made with <i class="fa fa-heart text-danger" aria-hidden="true"></i> by <a href="https://bitbucket.org/davidwlfreitas/" target="_blank" class="text-secondary">David Freitas</a>
            </li>

            <li class="nav-item text-white m-3">
                <a href="https://bitbucket.org/davidwlfreitas/blog-application/" target="_blank" class="btn btn-outline-secondary mt-1"><i class="fa fa-bitbucket" aria-hidden="true"></i> Fork me on BitBucket </a>
            </li>

        </ul>
    </div>
</nav>
