@php
  // It's a alternative solution for a Bug. It will be fixed in the next version.
  $post = $post->find(explode("/", Request::path())[3])
@endphp

@extends('admin.layouts.app')

@section('content')
    <p>@lang('posts.show') : {{ link_to_route('posts.show.home', route('posts.show.home', $post), $post->id) }}</p>

    @include('admin/posts/_thumbnail')

    {!! Form::model($post, ['route' => ['admin.posts.update', $post], 'method' =>'POST']) !!}
        @include('admin/posts/_form')

        <div class="pull-left">
            {{ link_to_route('admin.posts.index', __('forms.actions.back'), [], ['class' => 'btn btn-secondary']) }}
            {!! Form::submit(__('forms.actions.update'), ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

    {!! Form::model($post, ['method' => 'DELETE', 'route' => ['admin.posts.destroy', $post], 'class' => 'form-inline pull-right', 'data-confirm' => __('forms.posts.delete')]) !!}
        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> ' . __('posts.delete'), ['class' => 'btn btn-link text-danger', 'name' => 'submit', 'type' => 'submit']) !!}
    {!! Form::close() !!}
@endsection
