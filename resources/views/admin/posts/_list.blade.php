<table class="table table-striped table-sm table-responsive-md">
    <caption>{{ trans_choice('posts.count', count($posts)) }}</caption>
    <thead>
        <tr>
            <th>@lang('posts.attributes.title')</th>
            <th>@lang('posts.attributes.author')</th>
            <th>@lang('posts.attributes.posted_at')</th>
            <th><i class="fa fa-comments" aria-hidden="true"></i></th>
            <th><i class="fa fa-heart" aria-hidden="true"></i></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($posts as $post)
                @php
                    $author = App\Models\Admin::find($post->author_id);
                @endphp
                <td>{{ $post->title }}</td>
                <td>{{ $author['name'] }}</td>
                <td>{{ $post->posted_at }}</td>
                <td><span class="badge badge-pill badge-secondary">{{ $post->comments_count }}</span></td>
                <td><span class="badge badge-pill badge-secondary">{{ $post->likes_count }}</span></td>
                <td>
                    <a href="{{ route('admin.posts.edit', $post->id) }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>

                    {!! Form::model($post, ['method' => 'GET', 'route' => ['admin.posts.destroy', $post->id], 'class' => 'form-inline', 'data-confirm' => __('forms.posts.delete')]) !!}
                        <input type="text" name="id" value="{{ $post->id }}" style="display: none;">
                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-danger btn-sm', 'name' => 'submit', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="d-flex justify-content-center">
    {{ App\Models\Post::orderBy('id')->simplePaginate(10) }}
</div>
