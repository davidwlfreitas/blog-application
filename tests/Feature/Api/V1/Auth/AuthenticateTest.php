<?php

namespace Tests\Feature\Api\V1\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthenticateTest extends TestCase
{
    use RefreshDatabase;

    public function testAuthenticate()
    {
        $user = factory(User::class)->states('david')->create(['password' => bcrypt('555666')]);

        $res = $this->json('POST', '/api/v1/authenticate', [
                'email' => 'david@figured.nz',
                'password' => '555666'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'provider',
                    'provider_id',
                    'registered_at',
                    'comments_count',
                    'posts_count'
                ],
                'meta' => [
                    'access_token'
                ]
            ]);
    }

    public function testAuthenticateFail()
    {
        $user = factory(User::class)->states('david')->create(['password' => bcrypt('555666')]);

        $this->json('POST', '/api/v1/authenticate', [
                'email' => 'david@figured.nz',
                'password' => '777888'
            ])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'This action is unauthorized.'
            ]);
    }
}
