<?php

namespace Tests\Feature\Admin;

use App\Models\Post;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $david = factory(User::class)->states('david')->create();
        factory(User::class, 3)->create();
        factory(Post::class, 3)->create(['author_id' => $david->id]);

        $this->actingAsAdmin()
            ->get('/admin/users')
            ->assertStatus(200)
            ->assertSee('5 users')
            ->assertSee('3')
            ->assertSee('david@figured.nz')
            ->assertSee('David')
            ->assertSee('Name')
            ->assertSee('Email')
            ->assertSee('Registered at');
    }

    public function testEdit()
    {
        $david = factory(User::class)->states('david')->create();

        $this->actingAsAdmin()
            ->get("/admin/users/{$david->id}/edit")
            ->assertStatus(200)
            ->assertSee('David')
            ->assertSee('Show profile')
            ->assertSee('david@figured.nz')
            ->assertSee('Password confirmation')
            ->assertSee('Update')
            ->assertSee('Administrator');
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $params = $this->validParams();

        $this->actingAsAdmin()
            ->patch("/admin/users/{$user->id}", $params)
            ->assertRedirect("/admin/users/{$user->id}/edit");

        $this->assertDatabaseHas('users', $params);
        $this->assertEquals($params['email'], $user->refresh()->email);
    }

    /**
     * Valid params for updating or creating a resource
     *
     * @param  array  $overrides new params
     * @return array  Valid params for updating or creating a resource
     */
    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'David',
            'email' => 'david@figured.nz',
        ], $overrides);
    }
}
