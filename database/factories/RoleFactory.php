<?php

use Faker\Generator as Faker;
use App\Models\Role;

$factory->define(Role::class, function (Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->state(Role::class, 'admin', function ($faker) {
    return [
        'name' => 'admin'
    ];
});

$factory->state(Role::class, 'editor', function ($faker) {
    return [
        'name' => 'editor'
    ];
});
