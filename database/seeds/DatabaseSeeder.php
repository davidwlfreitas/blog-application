<?php

// use App\Models\Comment;
// use App\Models\MediaLibrary;
// use App\Models\Post;
use App\Models\Role;
use App\Models\Token;
use App\Models\User;
use App\Models\Admin;
use App\Models\RoleUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // MediaLibrary
        //MediaLibrary::firstOrCreate([]);

        // Users
        $user = User::firstOrCreate(
            ['email' => 'davidfreitas@figured.nz'],
            [
                'name' => 'David',
                'password' => Hash::make('555666'),
                'provider' => NULL,
                'provider_id' => NULL,
                'api_token' => NULL
            ]
        );

        // Users
        $user = Admin::firstOrCreate(
            ['email' => 'wilsonlopes@figured.nz'],
            [
                'name' => 'Wilson',
                'password' => Hash::make('777888'),
            ]
        );

        // API tokens
        // User::where('api_token', null)->get()->each->update([
        //     'api_token' => Token::generate()
        // ]);

        // Role User
        // db::query("INSERT INTO role_user (user_id, role_id) VALUES (1,2)");
        // RoleUser::firstOrCreate(['user_id' => 1, 'role_id' => 2]);

        // Posts
        // DB::collection('posts')->delete();
        //
        // DB::collection('posts')->insert([
        //     'title' => 'Hello World',
        //     'author_id' => $user->id
        // ],
        // [
        //     'posted_at' => now(),
        //     'content' => "
        //         Welcome to Laravel-blog !<br><br>
        //         Don't forget to read the README before starting.<br><br>
        //         Feel free to add a star on Laravel-blog on Github !<br><br>
        //         You can open an issue or (better) a PR if something went wrong."
        // ]);
        // $post = Post::firstOrCreate(
        //     [
        //         'title' => 'Hello World',
        //         'author_id' => $user->id
        //     ],
        //     [
        //         'posted_at' => now(),
        //         'content' => "
        //             Welcome to Laravel-blog !<br><br>
        //             Don't forget to read the README before starting.<br><br>
        //             Feel free to add a star on Laravel-blog on Github !<br><br>
        //             You can open an issue or (better) a PR if something went wrong."
        //     ]
        // );

        // Comments
        // Comment::firstOrCreate(
        //     [
        //         'author_id' => $user->id,
        //         'post_id' => $post->id
        //     ],
        //     [
        //         'posted_at' => now(),
        //         'content' => "Hey ! I'm a comment as example."
        //     ]
        // );

    }
}
